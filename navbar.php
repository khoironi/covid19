<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Dianosa online virus covid-19, cek kesehatan online, cara cek kesehatan, sistem pakar diagnosa virus covid-19">

  <meta name="author" content="khoironi">
  <title>COVID 19 | SISTEM DIAGNOSIS VIRUS COVID-19</title>
  <meta name="robots" content="index,follow">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115047942-12"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-115047942-12');
  </script>

  <!-- Favicons-->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

  <!-- GOOGLE WEB FONT -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

  <!-- BASE CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/menu.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/vendors.css" rel="stylesheet">

  <!-- YOUR CUSTOM CSS -->
  <link href="css/custom.css" rel="stylesheet">

  <!-- MODERNIZR MENU -->
  <script src="js/modernizr.js"></script>

</head>

<body>
  <div id="preloader">
    <div data-loader="circle-side"></div>
  </div><!-- /Preload -->

  <div id="loader_form">
    <div data-loader="circle-side-2"></div>
  </div><!-- /loader_form -->

  <header>
    <div class="container">
      <div class="row">
        <div class="col-3">
          <a href="index.php"><img src="img/apreksah.png" alt="" width="150" height="80" class="d-none d-md-block"><img src="img/apreksah.png" alt="" width="42" height="45" class="d-block d-md-none"></a>
          <i class="fas fa-heartbeat"></i>
        </div>
        <div class="col-9">
          <div id="social">
            <ul>
              <li><a href="#0"><i class="icon-facebook"></i></a></li>
              <li><a href="#0"><i class="icon-twitter"></i></a></li>
              <li><a href="#0"><i class="icon-google"></i></a></li>
              <li><a href="#0"><i class="icon-linkedin"></i></a></li>
            </ul>
          </div>
          <!-- /social -->
          <a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>
          <!-- /menu button -->
          <nav>
            <ul class="cd-primary-nav">
              <li><a href="question.php" class="animated_link">DIAGNOSA</a></li>
              <li><a href="prevention.html" class="animated_link">Prevention Tips</a></li>
              <li><a href="faq.html" class="animated_link">Faq</a></li>
              <li><a href="contacts.html" class="animated_link">Contact Us</a></li>

            </ul>
          </nav>
          <!-- /menu -->
        </div>
      </div>
    </div>
    <!-- /container -->

  </header>
  <!-- /Header -->