<?php include('navbar.php'); ?>

<div class="container">
    <div id="form_container">
        <div class="row no-gutters">
            <div class="col-lg-4">
                <div id="left_form">
                    <figure><img src="img/info_graphic_1.svg" alt="" width="100" height="100"></figure>
                    <h2>CORONAVIRUS <span>Kuesioner Kesehatan</span></h2>
                    <p>Bantu diri Anda dalam pengambilan keputusan apakah akan mencari nasihat medis profesional
                        atau tidak.</p>
                    <a href="question.php" class="btn_1 rounded yellow purchase">Diagnosa Sekarang</a>
                    <a href="question.php" class="btn_1 rounded mobile_btn yellow">Diagnosa Sekarang</a>
                    <a href="#0" id="more_info" data-toggle="modal" data-target="#more-info"><i class="pe-7s-info"></i></a>
                </div>
            </div>

            <div class="col-lg-8">
                <div id="wizard_container">

                    <!-- /top-wizard -->

                    <h2><i class="pe-7s-info"></i>Info</h2>
                    <p>jika anda mempunyai keahlian, pengalaman, dan keinginan untuk bantu tanggulangi COVID-19, gabung bersama kami menjadi Tim Relawan Penanggulangan COVID-19. Posisi yang tersdia meliputi relawan non-medis dan medis. <a href=""><span>Gabung Sekarang</span></a></p>
                    <!-- <figure><img src="img/solidarity.png" alt="" width="100" height="100"></figure> -->


                </div>
                <!-- /Wizard container -->
            </div>
        </div><!-- /Row -->
    </div><!-- /Form_container -->
</div>
<!-- /container -->
<?php include('footer.php'); ?>