<?php include('navbar.php'); ?>
<!-- /Header -->

<div class="container">
    <div id="form_container">
        <div class="row no-gutters">
            <div class="col-lg-4">
                <div id="left_form">
                    <figure><img src="img/info_graphic_1.svg" alt="" width="100" height="100"></figure>
                    <h2>CORONAVIRUS <span>Kuesioner Kesehatan</span></h2>
                    <p>Bantu diri Anda dalam pengambilan keputusan apakah akan mencari nasihat medis profesional
                        atau tidak.</p>
                    <!-- <a href="" class="btn_1 rounded yellow purchase" target="_parent">Master</a> -->
                    <!-- <a href="www.khoironi.net" class="btn_1 rounded mobile_btn yellow">Start Now!</a> -->
                    <a href="#0" id="more_info" data-toggle="modal" data-target="#more-info"><i class="pe-7s-info"></i></a>

                </div>
            </div>

            <div class="col-lg-8">
                <div id="wizard_container">
                    <div id="top-wizard">
                        <div id="progressbar"></div>
                        <span id="location"></span>
                    </div>
                    <!-- /top-wizard -->

                    <!-- <?php
                            session_start();
                            echo "<p>Hai, " . $_SESSION['nama'] . " (" . $_SESSION['umur'] . " th)</p>";
                            ?> -->
                    <form id="wrapped" method="post" action="" enctype="multipart/form-data" role="form">
                        <input id="website" name="website" type="text" value="">
                        <!-- Leave for security protection, read docs for details -->
                        <div id="middle-wizard">

                            <div class="step">
                                <?php
                                include('koneksi.php');
                                $kode = 'c1';


                                if (isset($_GET['kode'])) {
                                    $kode = $_GET['kode'];
                                }

                                $sql = "SELECT * from tb_pertanyaan WHERE kode_pertanyaan='$kode'";
                                $data = mysqli_query($connect, $sql);
                                $row = mysqli_fetch_assoc($data);
                                ?>

                                <label for="exampleFormControlInput1"></label>
                                <h3 class="main_question"><i class="arrow_right"></i><?php echo $row['isi_pertanyaan']; ?></h3>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">

                                            <div class="option">
                                                <?php
                                                include "fungsi.php";
                                                answer($kode);
                                                ?>



                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <!-- /row -->
                        </div>
                        <!-- /step-->

                        <!-- /step-->

                        <!-- /Start Branch ============================== -->


                        <!-- /difficulty in breathing > Yes ============================== -->

                        <!-- /step-->

                        <!-- /difficulty in breathing > No ============================== -->

                        <!-- /step-->


                        <!-- /step-->


                        <!-- /step-->


                        <!-- /step-->

                        <!-- /step-->

                        <div class="submit step" id="end">
                            <div class="summary">
                                <div class="wrapper">
                                    <h3>Thank your for your time<br><span id="name_field"></span>!</h3>
                                    <p>We will contat you shorly at the following email address <strong id="email_field"></strong> and if necessary take measures.</p>
                                </div>
                                <div class="text-center">
                                    <div class="form-group terms">
                                        <label class="container_check">Please accept our <a href="#" data-toggle="modal" data-target="#terms-txt">Terms and
                                                conditions</a> before Submit
                                            <input type="checkbox" name="terms" value="Yes" class="required">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /step last-->

                </div>
                <!-- /middle-wizard -->

                <!-- /bottom-wizard -->
                </form>
            </div>
            <!-- /Wizard container -->
        </div>
    </div><!-- /Row -->
</div><!-- /Form_container -->
</div>
<!-- /container -->
<?php include('footer.php'); ?>