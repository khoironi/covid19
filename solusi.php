<?php include('navbar.php'); ?>

<!-- /Header -->

<div class="container">
  <div id="form_container">
    <div class="row no-gutters">
      <div class="col-lg-4">
        <div id="left_form">
          <figure><img src="img/info_graphic_1.svg" alt="" width="100" height="100"></figure>
          <h2>CORONAVIRUS <span>Kuesioner Kesehatan</span></h2>
          <p>Bantu diri Anda dalam pengambilan keputusan apakah akan mencari nasihat medis profesional
            atau tidak.</p>
          <a href="question.php" class="btn_1 rounded yellow purchase">Diagnosa Ulang</a>
          <a href="question.php" class="btn_1 rounded mobile_btn yellow">Diagnosa Ulang</a>
          <a href="#0" id="more_info" data-toggle="modal" data-target="#more-info"><i class="pe-7s-info"></i></a>
        </div>
      </div>
      <div class="col-lg-8">
        <div id="wizard_container">
          <div id="top-wizard">
            <div id="progressbar"></div>
            <span id="location"></span>
          </div>
          <!-- /top-wizard -->
          <form method="post" action="solusi.php" enctype="multipart/form-data" role="form">




          </form>
          <form id="wrapped" method="post">
            <input id="website" name="website" type="text" value="">
            <!-- Leave for security protection, read docs for details -->
            <div id="middle-wizard">

              <div class="step">
                <h3 class="main_question"><i class="arrow_right"></i>
                  <?php
                  include('koneksi.php');
                  //$kode='m01';
                  // session_start();
                  // echo "<p>Nama : " . $_SESSION['nama'] . "</p>";
                  // echo "<p>Umur : " . $_SESSION['umur'] . "</p>";

                  if (isset($_GET['kode'])) {
                    $kode = $_GET['kode'];
                  }
                  ?>
                  <hr>
                  <p>Kamu merasa :</p>
                  <?php
                  include "fungsi.php";
                  solusi($kode);
                  ?>
                  <hr>
                  <?php
                  $sql = "SELECT * from tb_solusi WHERE kode_solusi='$kode'";
                  $data = mysqli_query($connect, $sql);
                  $row = mysqli_fetch_assoc($data);

                  if ($row['kode_solusi'] == "x-1" || $row['isi_solusi'] == "x-2" || $row['isi_solusi'] == "x-3" || $row['isi_solusi'] == "x-4" || $row['isi_solusi'] == "x-5") {
                    echo "<center><p><strong style='color:red'>Maaf, Apreksah belum memiliki data keluhan yang anda sampaikan !</strong></p></center><hr>";
                    echo "<center><p><strong style='color:red'>Basis data Apreksah terus diperbaharui agar mencakup lebih banyak gejala dan kemungkinan penyakit !</strong></p></center>";
                  ?>
                </h3>
                <!------------------------MASUKAN KEPADA SISTEM -------------------------------->
                <!-- <div class="card bg-dark">
                  <h5 class="card-header">Pengguna menambah fakta baru</h5>
                  <div class="card-body">
                    <form action="solusi.php" method="post">
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Pilih Jurusan :</label>
                        <select name="solusi" class="form-control" id="exampleFormControlSelect2">
                          <?php
                          include "koneksi.php";
                          $sql = "SELECT * from tb_solusi";
                          $data = mysqli_query($connect, $sql);
                          while ($row = mysqli_fetch_assoc($data)) {
                            if ($row['isi_solusi'] != "x-1" && $row['isi_solusi'] != "x-2" && $row['isi_solusi'] != "x-3" && $row['isi_solusi'] != "x-4" && $row['isi_solusi'] != "x-5") {
                              echo '<option value="' . $row["isi_solusi"] . '">' . $row["isi_solusi"] . '</option>';
                            }
                          }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlInput2">Masukan fakta:</label>
                        <input type="text" name="fakta" class="form-control" id="exampleFormControlInput1" placeholder="contoh : Suka memperbaiki komputer">
                      </div>
                      <input type="submit" class="btn btn-info" name="masukan">
                    </form>
                  </div>
                </div> -->
                <!------------------------MASUKAN KEPADA SISTEM -------------------------------->
              <?php
                  } else {
                    echo "<p>Hasil diagnosa kamu : <strong style='color:green'>" . $row['isi_solusi'] . "</strong></p>";
                    echo "<p>Terapi mandiri yang harus segera anda lakukan : <strong style='color:green'>" . $row['tips_solusi'] . "</strong></p>";
                  }

              ?>

              </div>
              <!-- /step-->








            </div>
            <!-- /middle-wizard -->
            <div id="bottom-wizard">
              <button type="button" name="backward" class="backward">Prev</button>
              <button type="button" name="forward" class="forward">Next</button>
              <button type="submit" name="process" class="submit">Submit</button>
            </div>
            <!-- /bottom-wizard -->
          </form>
        </div>
        <!-- /Wizard container -->
      </div>
    </div><!-- /Row -->
  </div><!-- /Form_container -->
</div>
<!-- /container -->
<?php include('footer.php'); ?>

<?php
include "koneksi.php";
if (!empty($_POST['masukan'])) {
  $fakta = $_POST['fakta'];
  $solusi = $_POST['solusi'];
  $oleh = $_SESSION['nama'];
  $status = "menunggu";

  $sql1 = "INSERT INTO tb_kesimpulan (solusi, fakta, oleh, status) VALUES ('$solusi', '$fakta', '$oleh', '$status')";
  if (mysqli_query($connect, $sql1)) {
    echo "<script>alert('Saran berhasil dimasukan, harus menunggu moderasi!'); window.location=('hapus-session.php');</script>";
    //echo "<script type='text/javascript'>window.location.replace('pakar-mode.php');</script>";
  } else  echo "<script>alert('gagal');</script>";
}

?>